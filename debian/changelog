fnonlinear (4041.82-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 
  * debian/control: Switch to virtual debhelper-compat (= 13)

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 08 Sep 2024 16:12:15 -0500

fnonlinear (4021.81-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Switch to virtual debhelper-compat (= 12)

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 26 Oct 2022 19:02:50 -0500

fnonlinear (4021.80-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 
  * debian/control: Switch to virtual debhelper-compat (= 11)
  * debian/compat: Removed
  * debian/control: Add Vcs-Browser: and Vcs-Git:
  * debian/control: Switch from cdbs to dh-r
  * debian/rules: Idem

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 20 Jul 2022 05:34:43 -0500

fnonlinear (3042.79-1) unstable; urgency=medium

  * New upstream release
  
  * debian/control: Set Standards-Version: to current version 
  * debian/control: Set Build-Depends: to current R version 
  * debian/compat: Set level to 9

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 16 Nov 2017 22:20:18 -0600

fnonlinear (3010.78-3) unstable; urgency=medium

  * Rebuilt under R 3.4.0 to update registration for .C() and .Fortran()
  
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 
  * debian/control: Add Depends: on ${misc:Depends}

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 05 Jun 2017 20:10:11 -0500

fnonlinear (3010.78-2) unstable; urgency=medium

  * debian/compat: Created
 
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 21 Jun 2016 07:17:39 -0500

fnonlinear (3010.78-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set Build-Depends: to current R version 

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 23 Jun 2013 19:44:24 -0500

fnonlinear (2160.77-2) unstable; urgency=low

  * debian/control: Set Build-Depends: to current R version 
  
  * (Re-)building with R 3.0.0

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 03 Apr 2013 16:26:35 -0500

fnonlinear (2160.77-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Change Depends to ${R:Depends}
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 10 Dec 2012 15:29:43 -0600

fnonlinear (2100.76-4) unstable; urgency=low

  * Rebuilt for R 2.14.0 so that a default NAMESPACE file is created

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 23 Oct 2011 15:58:12 -0500

fnonlinear (2100.76-3) unstable; urgency=low

  * Rebuilt for R 2.10.0 to work with new R-internal help file conversion

  * debian/control: Set (Build-)Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 04 Nov 2009 18:34:09 -0600

fnonlinear (2100.76-2) unstable; urgency=low

  * debian/control: Tighten (Build-)Depends on r-cran-base and r-cran-fgarch
							(Closes: #549562)

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 06 Oct 2009 06:24:14 -0500

fnonlinear (2100.76-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 01 Oct 2009 07:13:18 -0500

fnonlinear (290.75-2) unstable; urgency=low

  * debian/control: Tighten (Build-)Depends on r-cran-base and r-cran-fgarch
							(Closes: #526017)
  
 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 28 Apr 2009 20:43:52 -0500

fnonlinear (290.75-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set (Build-)Depends: on r-cran-fbasics to (>= 290.76)
  
  * debian/control: Changed Section: to new section 'gnu-r'
  
  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 3.8.1
  
 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 18 Apr 2009 11:07:03 -0500

fnonlinear (270.74-3) unstable; urgency=low

  * debian/control: Also tighten (Build-)Depends: on current r-cran-fgarch
  							(Closes: #504215)

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 02 Nov 2008 08:56:43 -0600

fnonlinear (270.74-2) unstable; urgency=low

  * debian/control: Tighten (Build-)Depends: on current r-cran-fbasics package
  							(Closes: #504215)

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 01 Nov 2008 13:53:01 -0500

fnonlinear (270.74-1) unstable; urgency=low

  * New upstream release

  * debian/control: Updated (Build-)Depends: and Suggests:
  
  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 28 Oct 2008 21:06:06 -0500

fnonlinear (260.72-3) unstable; urgency=low

  * debian/control: Tighten Build-Depends: on r-cran-robustbase that is implied 
    via Build-Depends: on r-cran-fbasics to ensure use of a version built after
    the gfortran transition					(Closes: #476820)

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 19 Apr 2008 10:08:16 -0500

fnonlinear (260.72-2) unstable; urgency=low

  * Rebuilding under the current R version
  * debian/control: Updated (Build-)Depends accordingly

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 14 Apr 2008 21:30:17 -0500

fnonlinear (260.72-1+b1) unstable; urgency=low

  * Rebuilding to aid the gfortran transition, no source or packaging change

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 10 Mar 2008 20:47:26 -0500

fnonlinear (260.72-1) unstable; urgency=low

  * Initial Debian release of new Rmetrics component package
  
 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 14 Nov 2007 21:34:22 -0600


